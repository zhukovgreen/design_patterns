import pytest

from .catalog import Catalog

def test_catalog():
    cat_1 = Catalog(1)
    cat_2 = Catalog(2)
    with pytest.raises(ValueError) as excinfo:
        cat_3 = Catalog(3)
    assert 'Param can be only' in str(excinfo)
    assert 'method_1' in cat_1.method()
    assert 'method_2' in cat_2.method()
