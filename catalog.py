"""Catalog design pattern."""


class Catalog:
    CATALOG = {1: '_method_1',
                2: '_method_2'}

    def __init__(self, param):
        self._param = param
        if param not in self.CATALOG:
            raise ValueError('Param can be only {0!r}'.format(self.CATALOG.keys()))

    def method(self):
        return getattr(self, self.CATALOG[self._param])()

    def _method_1(self):
        return 'Running method_1'

    def _method_2(self):
        return 'Running method_2'
