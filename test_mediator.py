import pytest

from .mediator import TestCategory, Database, TestManager, Reporter


def test_mediator():
    reporter = Reporter()
    database = Database()
    test_manager = TestManager()

    test_manager.reporter = reporter
    test_manager.database = database

    reporter.test_manager = test_manager
    database.test_manager = test_manager

    for _ in range(5):
        test_category = TestCategory()
        test_category.test_manager = test_manager

        test_manager.test_category = test_category

        test_category.setup()
        test_category.execute()
        test_category.tear_down()
