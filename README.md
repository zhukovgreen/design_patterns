My implementation of design patterns from https://en.wikipedia.org/wiki/Design_Patterns#Patterns_by_Type

# Environment
1. pip3.6 install pipenv
1. pipenv install --dev

# Tests
1. pipenv run pytest
