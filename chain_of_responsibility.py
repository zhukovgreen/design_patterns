import abc


class Handler(metaclass=abc.ABCMeta):
    def __init__(self, successor=None):
        if successor:
            self._successor = successor

    def handle(self, request):
        try:
            return self._handle(request)
        except (TypeError, ValueError):
            return self._successor.handle(request)

    @abc.abstractmethod
    def _handle(self, request):
        raise NotImplementedError('Should be implemented!')


class Handler1(Handler):
    def _handle(self, request):
        if request in [1, 3, 5]:
            return ('Handled in 1 with request {}'.format(request))
        else:
            raise ValueError


class Handler2(Handler):
    def _handle(self, request):
        if request in [0, 7, 9]:
            return ('Handled in 2 with request {}'.format(request))
        else:
            raise ValueError

class DefaultHandler(Handler):
    def _handle(self, request):
        raise NotImplementedError('No handler for this request {0!r}'.format(request))

class Manager:
    def __init__(self):
        self.handler = Handler1(Handler2(DefaultHandler()))

    def handle(self, request):
        return self.handler.handle(request)
