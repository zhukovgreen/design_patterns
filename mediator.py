import abc
import random
import time



class TestCategoryMeta(metaclass=abc.ABCMeta):
    def __init__(self):
        pass

    @abc.abstractmethod
    def setup(self):
        raise NotImplementedError

    @abc.abstractmethod
    def execute(self):
        raise NotImplementedError

    @abc.abstractmethod
    def tear_down(self):
        raise NotImplementedError


class ReporterMeta(metaclass=abc.ABCMeta):
    def __init__(self):
        pass

    @abc.abstractmethod
    def prepare(self):
        raise NotImplementedError

    @abc.abstractmethod
    def report(self):
        raise NotImplementedError


class DatabaseMeta(metaclass=abc.ABCMeta):
    def __init__(self):
        pass

    @abc.abstractmethod
    def insert(self):
        raise NotImplementedError

    @abc.abstractmethod
    def update(self):
        raise NotImplementedError


class TestManagerMeta(metaclass=abc.ABCMeta):
    def __init__(self):
        pass

    @abc.abstractmethod
    def prepare_reporting(self):
        raise NotImplementedError

    @abc.abstractmethod
    def publish_report(self):
        raise NotImplementedError


class TestCategory(TestCategoryMeta):
    def __init__(self):
        self._test_manager = None
        self._problem = 0

    @property
    def test_manager(self):
        return self._test_manager

    @property
    def problem(self):
        return self._problem

    @test_manager.setter
    def test_manager(self, value):
        self._test_manager = value

    @problem.setter
    def problem(self, value):
        self._problem = value

    def setup(self):
        print('Setting up test')
        time.sleep(0.1)
        self._test_manager.prepare_reporting()

    def execute(self):
        if not self._problem:
            print('Executing test')
            time.sleep(0.1)
        else:
            print('Test not executed. No tear down required')

    def tear_down(self):
        if not self._problem:
            time.sleep(0.1)
            self._test_manager.publish_report()
        else:
            print('Problem in setup. Test not executed')




class Reporter(ReporterMeta):
    def __init__(self):
        self._test_manager = None

    @property
    def test_manager(self):
        return self._test_manager

    @test_manager.setter
    def test_manager(self, test_manager):
        self._test_manager = test_manager

    def prepare(self):
        print("Reporter Class is preparing to report the results")
        time.sleep(0.1)

    def report(self):
        print("Reporting the results of Test")
        time.sleep(0.1)



class Database(DatabaseMeta):

    def __init__(self):
        self._test_manager = None

    @property
    def test_manager(self):
        return self._test_manager

    @test_manager.setter
    def test_manager(self, test_manager):
        self._test_manager = test_manager

    def insert(self):
        print("Inserting the execution begin status in the Database")
        time.sleep(0.1)
        # is to simulate a communication from Database to TestManager
        if random.randrange(1, 4) == 3:
            return False

    def update(self):
        print("Updating the test results in Database")
        time.sleep(0.1)



class TestManager:
    def __init__(self):
        self._reporter = None
        self._database = None
        self._test_category = None

    @property
    def reporter(self):
        return self._reporter

    @reporter.setter
    def reporter(self, reporter):
        self._reporter = reporter

    @property
    def database(self):
        return self._database

    @database.setter
    def database(self, database):
        self._database = database

    def publish_report(self):
        self._database.update()
        self._reporter.report()

    @property
    def test_category(self):
        return self._test_category

    @test_category.setter
    def test_category(self, test_category):
        self._test_category = test_category

    def prepare_reporting(self):
        rvalue = self._database.insert()
        if rvalue == -1:
            self._test_category.problem = 1
            self._reporter.prepare()
