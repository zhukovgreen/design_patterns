import pytest

from .chain_of_responsibility import Manager


def test_chain_of_responsibility():
    manager = Manager()
    responses = []
    for req in [1, 3, 5]:
        response = manager.handle(req)
        responses.append(response)
    assert all(['in 1' in response for response in responses])
    responses = []
    for req in [0, 7, 9]:
        response = manager.handle(req)
        responses.append(response)
    assert all(['in 2' in response for response in responses])

    with pytest.raises(NotImplementedError) as exceinfo:
        response = manager.handle(2)
    assert 'No handler' in str(exceinfo)
